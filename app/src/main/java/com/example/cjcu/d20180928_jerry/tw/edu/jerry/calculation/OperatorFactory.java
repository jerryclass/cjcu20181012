package com.example.cjcu.d20180928_jerry.tw.edu.jerry.calculation;

public class OperatorFactory {
    public static AOperator create(String operator)
    {
        AOperator ao1 = null;
        switch (operator)
        {
            case "+":
                ao1 = new Add();
                break;
            case "-":
                ao1 = new Sub();
                break;
            case "*":
                ao1 = new Mult();
                break;
            case "/":
                ao1 = new Div();
                break;
            default:
                break;
        }
        return ao1;
    }
}
